import tkinter as tk
from tkinter import ttk, Menu
from datetime import datetime
import pandas as pd
from web_tests import WebTestsSes
from web_tests_wo_age import WebTests
from send_emails import send_emails
import os
from log_session import log_session
from generate_bug_report import generate_bug_report
import shutil
from global_test_cases import *
from selenium import webdriver
from jira_class.create_issue import BugFields


class HtThreadedReport(WebTestsSes, WebTests):

    def __init__(self):

        self.url_list = []
        self.win = tk.Tk()
        self.win.minsize(900, 599)
        self.win.resizable(0, 0)
        self.chk_age = False
        self._mandatory_fields = []
        self.win.title("HT Reports")
        self.create_layout()
        self.menu_bar()
        self.req_fields = None


    def create_layout(self):
        # Add input field for URL
        self.enter_url_label = ttk.Label(self.win, text="Enter URL: ")
        self.enter_url_label.grid(column=1, row=0, sticky=tk.W, pady=10, padx=3)
        self.url = tk.StringVar()
        self.enter_url_txt_box = ttk.Entry(self.win, width=30, textvariable=self.url)
        self.enter_url_txt_box.grid(column=2, row=0, sticky=tk.W, padx=5)
        self.enter_url_txt_box.focus()

        # Add input field for company name
        self.company_name_label = ttk.Label(self.win, text="Enter Company Name: ")
        self.company_name_label.grid(column=1, row=1, sticky=tk.W, pady=10, padx=3)
        self.company_name = tk.StringVar()
        self.company_name_txt_box = ttk.Entry(self.win, width=30, textvariable=self.company_name)
        self.company_name_txt_box.grid(column=2, row=1, sticky=tk.W, padx=5)

        # Add input field for company email
        self.company_email_label = ttk.Label(self.win, text="Enter Report Sender's Outlook Email: ")
        self.company_email_label.grid(column=1, row=2, sticky=tk.W, pady=10, padx=3)
        self.company_email = tk.StringVar()
        self.company_email_txt_box = ttk.Entry(self.win, width=30, textvariable=self.company_email)
        self.company_email_txt_box.grid(column=2, row=2, sticky=tk.W, padx=5)

        # Add input field for company email's password
        self.company_email_password_label = ttk.Label(self.win, text="Enter outlook password: ")
        self.company_email_password_label.grid(column=1, row=3, sticky=tk.W, pady=10, padx=3)
        self.company_email_password = tk.StringVar()
        self.company_email_password_txt_box = ttk.Entry(self.win, width=30, textvariable=self.company_email_password, show="*")
        self.company_email_password_txt_box.grid(column=2, row=3, sticky=tk.W, padx=5)

        # Add input field for company email
        self.receiver_email_label = ttk.Label(self.win, text="Enter Receiver's Email Address: ")
        self.receiver_email_label.grid(column=1, row=4, sticky=tk.W, pady=10, padx=3)
        self.receiver_email = tk.StringVar()
        self.receiver_email_txt_box = ttk.Entry(self.win, width=30, textvariable=self.receiver_email)
        self.receiver_email_txt_box.grid(column=2, row=4, sticky=tk.W, padx=5)

        # if site is password protected section
        self.is_protected_label = ttk.Label(self.win, text="Select if Site is Password Protected: ")
        self.is_protected_label.grid(column=1, row=5, sticky=tk.W, pady=10, padx=3)
        self.is_protected = tk.IntVar()
        self.is_protected_chk_box = tk.Checkbutton(self.win, variable=self.is_protected,
                                                   command=self.is_protected_clicked)
        self.is_protected_chk_box.deselect()
        self.is_protected_chk_box.grid(column=2, row=5, sticky=tk.W, padx=5)

        # Add input field for username
        self.username_label = ttk.Label(self.win, text="Enter Username: ")
        self.username_label.grid(column=1, row=6, sticky=tk.W, pady=10, padx=3)
        self.username = tk.StringVar()
        self.username_txt_box = tk.Entry(self.win, width=30, textvariable=self.username, state="disabled")
        self.username_txt_box.grid(column=2, row=6, sticky=tk.W, padx=5)

        # Add input field for password
        self.password_label = ttk.Label(self.win, text="Enter Password: ")
        self.password_label.grid(column=1, row=7, sticky=tk.W, pady=10, padx=3)
        self.password = tk.StringVar()
        self.password_txt_box = tk.Entry(self.win, width=30, textvariable=self.password, state="disabled")
        self.password_txt_box.grid(column=2, row=7, sticky=tk.W, padx=5)

        # if age validation page is there
        self.is_chk_age_check_label = ttk.Label(self.win, text="Select if Age Check Page is There")
        self.is_chk_age_check_label.grid(column=1, row=8, sticky=tk.W, pady=10, padx=3)
        self.is_chk_age_page = tk.IntVar()
        self.is_chk_age_box = tk.Checkbutton(self.win, variable=self.is_chk_age_page,
                                             command=self.is_chk_age_page_clicked)
        self.is_chk_age_box.deselect()
        self.is_chk_age_box.grid(column=2, row=8, sticky=tk.W, padx=5)

        ttk.Separator(self.win, orient='vertical').grid(column=3, columnspan=2, row=0, rowspan=20, sticky='ns', padx=50)
        ttk.Separator(self.win, orient='horizontal').grid(column=1, columnspan=12, row=20, sticky='we', pady=10, padx=5)

        ######################## jira options ###########################
        # if site is password protected section
        self.create_jira_ticket = ttk.Label(self.win, text="Select To Create / Update JIRA Tickets: ")
        self.create_jira_ticket.grid(column=5, row=0, sticky=tk.W, pady=10, padx=3)
        self.jira_ticket = tk.IntVar()
        self.create_jira_ticket_chk_box = tk.Checkbutton(self.win, variable=self.jira_ticket,
                                                         command=self.create_jira_ticket_clicked)
        self.create_jira_ticket_chk_box.deselect()
        self.create_jira_ticket_chk_box.grid(column=6, row=0, sticky=tk.W, padx=5)

        # add field for jira url
        self.jira_url_label = ttk.Label(self.win, text="Enter JIRA URL: ")
        self.jira_url_label.grid(column=5, row=1, sticky=tk.W, pady=10, padx=3)
        self.jira_url = tk.StringVar()
        self.jira_url_txt_box = ttk.Entry(self.win, width=30, textvariable=self.jira_url, state="disabled")
        self.jira_url_txt_box.grid(column=6, row=1, sticky=tk.W, padx=5)

        # Add input field for username
        self.jira_username_label = ttk.Label(self.win, text="Enter JIRA Username: ")
        self.jira_username_label.grid(column=5, row=2, sticky=tk.W, pady=10, padx=3)
        self.jira_username = tk.StringVar()
        self.jira_username_txt_box = tk.Entry(self.win, width=30, textvariable=self.jira_username, state="disabled")
        self.jira_username_txt_box.grid(column=6, row=2, sticky=tk.W, padx=5)

        # Add input field for password
        self.jira_password_label = ttk.Label(self.win, text="Enter JIRA Password: ")
        self.jira_password_label.grid(column=5, row=3, sticky=tk.W, pady=10, padx=3)
        self.jira_password = tk.StringVar()
        self.jira_password_txt_box = tk.Entry(self.win, width=30, textvariable=self.jira_password, state="disabled",
                                              show="*")
        self.jira_password_txt_box.grid(column=6, row=3, sticky=tk.W, padx=5)

        # Add input field for project id
        self.jira_project_id = ttk.Label(self.win, text="Enter JIRA Project ID: ")
        self.jira_project_id.grid(column=5, row=4, sticky=tk.W, pady=10, padx=3)
        self.project_id = tk.StringVar()
        self.jira_project_id_txt_box = tk.Entry(self.win, width=30, textvariable=self.project_id, state="disabled")
        self.jira_project_id_txt_box.grid(column=6, row=4, sticky=tk.W, padx=5)

        # Add input field for assignee name
        self.jira_assignee_name = ttk.Label(self.win, text="Enter Assignee Name: ")
        self.jira_assignee_name.grid(column=5, row=5, sticky=tk.W, pady=10, padx=3)
        self.assignee_name = tk.StringVar()
        self.jira_assignee_name_txt_box = tk.Entry(self.win, width=30, textvariable=self.assignee_name, state="disabled")
        self.jira_assignee_name_txt_box.grid(column=6, row=5, sticky=tk.W, padx=5)

        self.action = ttk.Button(self.win, text="Run Tests", command=self.run_tests, state='normal')
        # self.action.place(x=350, y=450)
        self.action.grid(column=2, row=30)
        self.get_jira_fields_btn = ttk.Button(self.win, text="Get Jira Fields", command=self.get_jira_details,
                                              state='disabled')

        self.get_jira_fields_btn.grid(column=6, row=30)

        self.test_status = ttk.Label(self.win, text="Test completion message will be displayed here")
        self.test_status.grid(column=4, row=30, sticky=tk.E)

        # Validation error message
        self.field_err = ttk.Label(self.win, text="One or more mandatory fields are not filled")

    def get_jira_details(self):
        self.req_fields = BugFields(self.jira_url.get(), self.project_id.get(), self.jira_username.get(), self.jira_password.get())
        self._mandatory_fields = self.req_fields.mandatory_data_dict
        row = 6
        if len(self._mandatory_fields) > 0:
            for k, v in self._mandatory_fields.items():
                for key, value in v.items():
                    labelTop = tk.Label(self.win, text=key)
                    labelTop.grid(column=5, row=row, sticky=tk.W, pady=10, padx=3)
                    globals()[key] = ttk.Combobox(self.win, values=value, width=30)
                    globals()[key].grid(column=6, row=row, sticky=tk.W, padx=5)
                row = row + 1
        else:
            self.mandate_fields = ttk.Label(self.win, text="Fields required by this tool are already present")
            self.mandate_fields.grid(column=5, row=row, sticky=tk.W, padx=10, pady=3)
        self.action.configure(state='normal')
        self.get_jira_fields_btn.configure(state='disabled')

    def menu_bar(self):
        menu_bar = Menu(self.win)
        self.win.config(menu=menu_bar)
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label="New", command=self._new)
        file_menu.add_command(label="Exit", command=self._quit)
        menu_bar.add_cascade(label="File", menu=file_menu)

    def is_protected_clicked(self):
        if self.is_protected.get() == 1:
            self.username_txt_box.configure(state="normal")
            self.password_txt_box.configure(state="normal")
        else:
            self.username_txt_box.configure(state="disabled")
            self.password_txt_box.configure(state="disabled")

    def is_chk_age_page_clicked(self):
        if self.is_chk_age_page.get() == 1:
            self.chk_age = True
        else:
            self.chk_age = False

    def create_jira_ticket_clicked(self):
        if self.jira_ticket.get() == 1:
            self.jira_username_txt_box.configure(state="normal")
            self.jira_password_txt_box.configure(state="normal")
            self.jira_project_id_txt_box.configure(state="normal")
            self.jira_assignee_name_txt_box.configure(state="normal")
            self.jira_url_txt_box.configure(state="normal")
            self.action.configure(state='disabled')
            self.get_jira_fields_btn.configure(state='normal')
            if len(self._mandatory_fields) > 0:
                for k, v in self._mandatory_fields.items():
                    for key, value in v.items():
                        globals()[key].configure(state="normal")
        else:
            self.jira_username_txt_box.configure(state="disabled")
            self.jira_password_txt_box.configure(state="disabled")
            self.jira_project_id_txt_box.configure(state="disabled")
            self.jira_assignee_name_txt_box.configure(state="disabled")
            self.jira_url_txt_box.configure(state="disabled")
            self.action.configure(state='normal')
            self.get_jira_fields_btn.configure(state='disabled')
            if len(self._mandatory_fields) > 0:
                for k, v in self._mandatory_fields.items():
                    for key, value in v.items():
                        globals()[key].configure(state="disabled")

    def color_negative_red(self, val):
        """
        Takes a scalar and returns a string with
        the css property `'color: red'` for negative
        strings, black otherwise.
        """
        errors = [
            "Missing", "Meta description is missing", "Page title is missing", "Missing og tag",
            "Missing canonical url", "Missing back to top", "Not accessible", "Timeout", "No node URLs on this page",
            "Back to top is missing"
        ]
        color = 'red' if val in errors or '[<' in str(val) or "['" in str(val) or \
                         (isinstance(val, int) and (val > 400 or val in [301, 302])) \
            else 'black'
        # color = 'red' if '[<' in val else 'black'
        return 'color: %s' % color

    def validate_fields(self):
        if not len(self.url.get()) > 0:
            self.error = 1
        if not len(self.company_name.get()) > 0:
            self.error = 1
        if not len(self.company_email.get()) > 0:
            self.error = 1
        if not len(self.receiver_email.get()) > 0:
            self.error = 1
        if not len(self.company_email_password.get()) > 0:
            self.error = 1
        if self.is_protected == 1:
            if not len(self.username) > 0:
                self.error = 1
            if not len(self.password) > 0:
                self.error = 1

    def run_tests(self):
        self.error = 0
        self.validate_fields()
        if self.error == 1:
            self.field_err.grid(column=4, row=39, sticky=tk.E)
            self.field_err.configure(foreground="red")
        else:
            self.field_err.grid_forget()
            self.action.configure(state='disabled')
            self.url_list.append(self.url.get())
            parsed = urlparse(self.url.get())
            host_name = parsed.netloc
            auth_uname = self.username.get()
            auth_pass = self.password.get()
            host_scheme = parsed.scheme
            columnOrder = ["URL", "Response_code", "Redirection_depth", "Redirected_to_url", "Meta title",
                           "Company Name in page title", "Duplicate title on this page", "Meta description",
                           "Duplicate meta description on this page",
                           "Company Name in meta description", "List of Node urls on page",
                           "External links open in same window", "External link without new window message",
                           "Img tags without alt attribute", "Img tags with empty or none alt text", "Back to top",
                           "Missing File Type", "Links without content", "if i tag present",
                           "if b tag present", "if font tag present", "File size missing"
                , "og tags", "Number of h1 on page", "Img with alt LOGO", "Has headings"
                , "More than one title", "Meaningless Phrases", "Canonical URL", "Image size greater than 50KB"
                , "Home page link is present", "Empty p tags are", "Consecutive br tags",
                           "File getting open in same tab", "Google Site Verification Meta Tag", "GTM code is present",
                           "Natural language", "Skip to main content", "Form controls not readable by screen reader",
                           "Tags containing in-line style", "Style tags present on page",
                           "Heading longer than 10 words in length", "Space before a </p> tag",
                           "Paragraph longer than 100 words", "Internal links getting open in a new window",
                           "Delivered by Investis", "File size not per Investis Standard", "Disclaimer is present",
                           "Row and column headers and role are missing", "File/Directory with special character",
                           "Img with lower case initial", "PDF Document Title", "H tags hierarchy", "PDF Document Title"]

            start_time = datetime.now().strftime("%H:%M:%S")

            num_of_url = 0
            checked_css = []  # maintains the list of css files which are already validated
            url = self.url_list[num_of_url]
            general = {}
            general_result = []
            sitemap_node_url = check_site_map(url, self.username.get(), self.password.get(), host_scheme)
            general.update(sitemap_node_url)
            get_resp = GetResponse()
            base_url = get_resp.form_url(url, self.username.get(), self.password.get(), host_scheme)

            if self.chk_age:
                driver = webdriver.Chrome(executable_path=r".\driver\chromedriver.exe")
            else:
                driver = webdriver.Chrome(executable_path=r".\driver\chromedriver.exe")
                # driver = webdriver.PhantomJS(executable_path=r".\phantomjs-2.1.1-windows\bin\phantomjs.exe")

            driver.maximize_window()
            # try:
            while num_of_url < len(self.url_list):
                # print(self.url_list)
                if self.url_list[num_of_url] != "" and "/void(0)" not in self.url_list[num_of_url].lower() and \
                        "/search-result" not in self.url_list[num_of_url].lower() and "/ics" not in self.url_list[num_of_url].lower() \
                        and "/taxonomy/" not in self.url_list[num_of_url].lower() \
                        and "rss.xml" not in self.url_list[num_of_url].lower()\
                        and "readme.md"not in self.url_list[num_of_url].lower()\
                        and ".png" not in self.url_list[num_of_url].lower()\
                        and ".jpg" not in self.url_list[num_of_url].lower()\
                        and ".jpeg" not in self.url_list[num_of_url].lower()\
                        and ".gif" not in self.url_list[num_of_url].lower()\
                        and "void(0)" not in self.url_list[num_of_url].lower():
                    # print("Checking url " + str(num_of_url) + ": " + str(self.url_list[num_of_url]))
                    try:
                        driver.get(get_resp.form_url(self.url_list[num_of_url], auth_uname, auth_pass, host_scheme))
                        if self.chk_age:
                            if num_of_url == 0:
                                ses = log_session(driver, get_resp.form_url(self.url_list[num_of_url], auth_uname, auth_pass,
                                                                            host_scheme))
                            resp = get_resp.get_url(self.url_list[num_of_url], auth_uname, auth_pass, host_scheme, ses)
                        else:
                            resp = get_resp.get_url(self.url_list[num_of_url], auth_uname, auth_pass, host_scheme, None)
                        try:

                            if num_of_url == 0:
                                try:
                                    pr_access = {}
                                    soup = bs(resp.text, "html.parser")
                                    footer = soup.find("footer")
                                    pr_access = check_privacy_presence(footer, parsed)
                                    sitemap_html = check_sitemap_html_presence(footer, parsed)
                                    general.update(pr_access)
                                    general.update(sitemap_html)
                                except:
                                    pass

                            if self.chk_age:
                                self.url_list = self.find_all_urls_ses(resp, base_url, self.url_list, num_of_url,
                                                                       self.company_name.get(),
                                                                       host_name, host_scheme, ses, driver,
                                                                       checked_css)
                            else:
                                self.url_list = self.find_all_urls(resp, base_url, self.url_list, num_of_url,
                                                                   self.company_name.get(),
                                                                   host_name, host_scheme, driver,
                                                                   checked_css)
                        except:
                            # print("Failed")
                            pass
                    except:
                        pass
                # if num_of_url == 3:
                #     break
                num_of_url = num_of_url + 1

            driver.quit()
            end_time = datetime.now().strftime("%H:%M:%S")
            time_taken = datetime.strptime(str(end_time), "%H:%M:%S") - datetime.strptime(str(start_time), "%H:%M:%S")

            df = pd.DataFrame(self.final_result, columns=columnOrder)

            time_stamp = ''.join(e for e in str(datetime.now()) if e.isalnum())
            cwd = os.getcwd()
            file_path = cwd + "\\reports" + "\\" + self.company_name.get()
            if not os.path.exists(file_path):
                os.mkdir(file_path)

            file_path = file_path + "\\" + time_stamp[:12]
            if not os.path.exists(file_path):
                os.mkdir(file_path)

            fname = self.company_name.get().replace(" ", "") + "_" + time_stamp[:12] + ".xlsx"
            filename = file_path + "\\" + fname
            with open(filename, "w") as f:
                f.close()
            writer = pd.ExcelWriter(filename, engine="xlsxwriter")  # , options={'strings_to_urls': False})
            df.to_excel(writer, sheet_name="Site_results")

            writer.save()

            def navigate_and_rename(src):
                for item in os.listdir(src):
                    s = os.path.join(src, item)
                    if os.path.isdir(s):
                        navigate_and_rename(s)
                    elif s.endswith(fname):
                        file_copy = fname[:-5] + "_copy.xlsx"
                        shutil.copy(s, os.path.join(src, "{filename}".format(filename=file_copy)))
                return file_copy

            fc = navigate_and_rename(file_path)
            fc = file_path + "\\" + fc
            dfs = pd.read_excel(fc, sheet_name="Site_results")
            dfs = dfs.drop(["Unnamed: 0"], axis=1)

            s = dfs.style.applymap(self.color_negative_red)
            writer = pd.ExcelWriter(filename, engine="xlsxwriter")
            s.to_excel(writer, sheet_name="Site_results", index=False)

            if len(self.external_url_result) > 0:
                external_url_status_df = pd.DataFrame(self.external_url_result, columns=['URL', 'Response Code'])
                external_url_status_df.to_excel(writer, sheet_name="External URL Http Response Code")

            # create a dataframe to store the list of URLs present on a page
            on_page_url_df = pd.DataFrame(self.urls_on_page, columns=['URL', 'URLs on this page'])
            on_page_url_df.to_excel(writer, sheet_name="URLs present on each page")
            general_result.append(general)
            if len(general_result) > 0:
                general_data = pd.DataFrame(general_result,
                                            columns=['Node URLs', "Privacy Statement", "Accessibility Statement",
                                                     "Sitemap HTML"])
                general_data.to_excel(writer, sheet_name="General")
            if len(self.error_result) > 0:
                df_error_result = pd.DataFrame(self.error_result)
                df_error_result.to_excel(writer, sheet_name="XHTML Validation errors", index=False)

            if len(self.warning_result) > 0:
                df_warnings_result = pd.DataFrame(self.warning_result)
                df_warnings_result.to_excel(writer, sheet_name="XHTML Warnings", index=False)
            # df_css_error_results = pd.DataFrame(self.css_result)
            # df_css_error_results.to_excel(writer, sheet_name="CSS Validation errors", index=False)
            writer.save()

            file, filename = generate_bug_report(dfs, time_stamp[:12], file_path)

            if self.jira_ticket.get():
                url = self.jira_url.get()
                jira_url = url

                jira_data = {"assignee": self.assignee_name.get()}
                for k, v in self._mandatory_fields.items():
                    jira_data.update({k: list(v.values())[0][globals()[list(v.keys())[0]].current()]})

                self.req_fields.create_update_close_bug(jira_url, self.jira_username.get(), self.jira_password.get(),
                                                   self.project_id.get(), file, file_path, jira_data)

            # this try except block is for sending email to the recipient list
            try:
                send_emails(file_path, str(self.company_email.get()), str(self.receiver_email.get()), str(self.company_email_password.get())
                            , self.company_name.get())
            except:
                email_error = ttk.Label(self.win, text="Unable to send email")
                email_error.grid(column=4, row=32, sticky=tk.E)

            self.test_status.configure(text="Test completed")
            started_at = ttk.Label(self.win, text="Started At: " + str(start_time))
            started_at.grid(column=4, row=31, sticky=tk.E)

            ended_at = ttk.Label(self.win, text="Ended At: " + str(end_time))
            ended_at.grid(column=4, row=32, sticky=tk.E)

            total_time_taken = ttk.Label(self.win, text="Total Time Taken: " + str(time_taken))
            total_time_taken.grid(column=4, row=33, sticky=tk.E)

            try:
                os.remove(fc)
            except OSError:
                pass

    # def write(self, s):
    #     self.output.insert(tk.END, s)
    #     self.win.update_idletasks()

    def flush(self):
        pass

    def _quit(self):
        self.win.quit()
        self.win.destroy()
        exit()

    def _new(self):

        self.flush()
        self.win.destroy()
        # sys.stdout = HTR = HtThreadedReport()
        HTR = HtThreadedReport()
        HTR.win.mainloop()
        self.__init__()


if __name__ == "__main__":
    # sys.stdout = HTR = HtThreadedReport()
    HTR = HtThreadedReport()
    HTR.win.mainloop()
