import pandas as pd


def check_response_code(rc_df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        if rc_df["Response_code"][x] != 200 and rc_df["Response_code"][x] != 301 and rc_df["Response_code"][x] != 302:
            result["URL"] = rc_df["URL"][x]
            result["Response_code Code"] = rc_df["Response_code"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Response Code", index=False)


def check_node_urls(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if len(df["List of Node urls on page"][x]) != 0 and df["List of Node urls on page"][0] != "None":
                if len(df["List of Node urls on page"]) == 1 and df["URL"][x] != df["List of Node urls on page"][0]:
                    result["URL"] = df["URL"][x]
                    result["List of Node urls on page"] = df["List of Node urls on page"][x]
                    result_list.append(result)
                else:
                    result["URL"] = df["URL"][x]
                    result["List of Node urls on page"] = df["List of Node urls on page"][x]
                    result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Node URLs", index=False)


def check_link_wo_content(df, length, writer):
    x = 0

    result_list = []
    # print(len(df["Links without content"][x]))
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if len(df["Links without content"][x]) != 0 and df["Links without content"][x] != "None"  and df["Links without content"][x] !="It is a file":
                result["URL"] = df["URL"][x]
                result["Links without content"] = df["Links without content"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Links without content", index=False)


def check_img_alt(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if len(df["Img tags without alt attribute"][x]) != 0 and df["Img tags without alt attribute"][x] != "None":
                result["URL"] = df["URL"][x]
                result["Img tags without alt attribute"] = df["Img tags without alt attribute"][x]
                result_list.append(result)
            if len(df["Img tags with empty or none alt text"][x]) != 0 and df["Img tags with empty or none alt text"][x] != "None" and df["Img tags with empty or none alt text"][x] !="It is a file":
                result["URL"] = df["URL"][x]
                result["Img tags with empty or none alt text"] = df["Img tags with empty or none alt text"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Img tags without alt attribute", index=False)


def check_external_links(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if (len(df["External links open in same window"][x]) != 0 \
                    and df["External links open in same window"][x] != "None" and \
                    df["External links open in same window"][x] != "It is a file") or \
                (len(df["External link without new window message"][x]) != 0
                 and df["External link without new window message"][x] != "None"
                 and df["External link without new window message"][x] != "It is a file"):
                result["URL"] = df["URL"][x]
                if len(df["External links open in same window"][x]) != 0:
                    result["External links open in same window"] = df["External links open in same window"][x]
                elif len(df["External link without new window message"][x]) != 0:
                    result["External link without new window message"] = df["External link without new window message"][
                        x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="External Link Issues", index=False)


def check_file_size_type(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if len(df["Missing File Type"][x]) != 0 and df["Missing File Type"][x] != "None" and df["Missing File Type"][x] != "It is a file":
                result["URL"] = df["URL"][x]
                result["Missing File Type"] = df["Missing File Type"][x]
                result_list.append(result)
            if len(df["File size missing"][x]) != 0 and df["File size missing"][x] != "None" and df["File size missing"][x] != "It is a file":
                result["URL"] = df["URL"][x]
                result["File size missing"] = df["File size missing"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="Missing File Type or Size", index=False)


def check_no_of_titles(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if df["More than one title"][x] == "True":
                result["URL"] = df["URL"][x]
                result["More than one title"] = df["More than one title"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="More than one title", index=False)


def check_no_of_h1_tags(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower()  and ".doc" not in \
                str(url).lower()  and ".docx" not in str(url).lower():
            if df["Number of h1 on page"][x] > 1:
                result["URL"] = df["URL"][x]
                result["Number of h1 on page"] = df["Number of h1 on page"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="More than one h1 tags", index=False)


def check_canonical_urls(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if df["Canonical URL"][x] != "Present" and df["Canonical URL"][x] != "It is a file":
                result["URL"] = df["URL"][x]
                result["Canonical URL"] = df["Canonical URL"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="Missing Canonical URLs", index=False)


def check_i_b_font_tags(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if df["if i tag present"][x] is True:
                result["URL"] = df["URL"][x]
                result["if i tag present"] = df["if i tag present"][x]
                result_list.append(result)
            if df["if b tag present"][x] is True:
                result["URL"] = df["URL"][x]
                result["if b tag present"] = df["if b tag present"][x]
                result_list.append(result)
            if df["if font tag present"][x] is True:
                result["URL"] = df["URL"][x]
                result["if font tag present"] = df["if font tag present"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="Pages with i or b or font tags", index=False)


def check_pages_missing_page_title(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if df["Meta title"][x] == "Page title is missing":
                result["URL"] = df["URL"][x]
                result["Meta title"] = df["Meta title"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="Page missing page title", index=False)


def check_company_name_in_page_title(df, length, writer):
    x = 0

    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
                and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
            url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
            if df["Company Name in page title"][x] == "Missing":
                result["URL"] = df["URL"][x]
                result["Company Name in page title"] = df["Company Name in page title"][x]
                result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        # print(res_df)
        res_df.to_excel(writer, sheet_name="Company Name in page title", index=False)


# def check_company_name_in_meta_desc(df, length, writer):
#     x = 0
#
#     result_list = []
#     while x < length:
#         result = {}
#         url = df["URL"][x]
#         if ".pdf" not in str(url).lower() and ".mp3" not in str(url).lower() and ".mp4" not in str(url).lower() \
#                 and ".xls" not in str(url).lower() and ".xlsx" not in str(url).lower() and ".mov" not in str(
#             url).lower() and ".txt" not in str(url).lower() and ".zip" not in str(url).lower():
#             if df["Company Name in meta description"][x] == "Missing":
#                 result["URL"] = df["URL"][x]
#                 result["Company Name in meta description"] = df["Company Name in meta description"][x]
#                 result_list.append(result)
#         x = x + 1
#
#     if len(result_list) > 0:
#         res_df = pd.DataFrame(result_list)
#         # print(res_df)
#         res_df.to_excel(writer, sheet_name="Company Name in meta descp", index=False)


def check_duplicate_page_title(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Duplicate title on this page"][x] == "Present":
            result["URL"] = url
            result["Duplicate page title"] = df["Meta title"][x]
            result_list.append(result)
        x= x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Duplicate page title", index=False)


def check_duplicate_meta_desc(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Duplicate meta description on this page"][x] == "Present":
            result["URL"] = url
            result["Duplicate meta description"] = df["Meta description"][x]
            result_list.append(result)
        x= x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Duplicate meta description", index=False)


def check_link_to_home_page(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Home page link is present"][x] != "Present" and df["Home page link is present"][x] != "Not required for homepage" and df["Home page link is present"][x] != "It is a file":
            result["URL"] = url
            result["Missing link to Homepage"] = df["Home page link is present"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Missing link to Homepage", index=False)


def check_file_getting_open_in_same_tab(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["File getting open in same tab"][x] != "None" and df["File getting open in same tab"][x] != "It is a file":
            result["URL"] = url
            result["File getting open in same tab"] = df["File getting open in same tab"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="File getting open in same tab", index=False)


def check_google_meta_and_gtm(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Google Site Verification Meta Tag"][x] == "Missing on this page" or df["GTM code is present"][x] == "No":
            result["URL"] = url
            result["Google Site Verification Meta Tag"] = df["Google Site Verification Meta Tag"][x]
            result["GTM code is present"] = df["GTM code is present"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Google meta and GTM code", index=False)


def check_page_natural_language(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Natural language"][x] != "Defined" and df["Natural language"][x] != "It is a file":
            result["URL"] = url
            result["Html tag missing lang attr"] = df["Natural language"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Page natural lang not defined", index=False)


def check_form_controls(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Form controls not readable by screen reader"][x] != "No missing labels" and df["Form controls not readable by screen reader"][x] != "It is a file":
            result["URL"] = url
            result["Fields not readable by screen reader"] = df["Form controls not readable by screen reader"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Not readable by screen reader", index=False)


def check_internal_links_opening_in_new_tab(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Internal links getting open in a new window"][x] != "No such internal links" and df["Internal links getting open in a new window"][x] != "It is a file":
            result["URL"] = url
            result["Internal links open in a new window"] = df["Internal links getting open in a new window"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Internal link open in a new tab", index=False)


def check_delivered_by(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["Delivered by Investis"][x] != "Present" and df["Delivered by Investis"][x] != "It is a file":
            result["URL"] = url
            result["Delivered by Investis"] = df["Delivered by Investis"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Delivered by Investis", index=False)


def check_special_chars_in_path(df, length, writer):
    x = 0
    result_list = []
    while x < length:
        result = {}
        url = df["URL"][x]
        if df["File/Directory with special character"][x] != "None" and df["File/Directory with special character"][x] != "It is a file":
            result["URL"] = url
            result["File/Directory with special char"] = df["File/Directory with special character"][x]
            result_list.append(result)
        x = x + 1

    if len(result_list) > 0:
        res_df = pd.DataFrame(result_list)
        res_df.to_excel(writer, sheet_name="Path or name with special char", index=False)


def generate_bug_report(df, time_stamp, file_path):
    length = len(df)
    filename = "Bugs_"+time_stamp+".xlsx"
    file = file_path + "\\" + filename
    with open(file, "w") as f:
        f.close()
    writer = pd.ExcelWriter(file, engine="xlsxwriter", options={'strings_to_urls': False})

    check_response_code(df, length, writer)
    check_node_urls(df, length, writer)
    check_link_wo_content(df, length, writer)
    check_img_alt(df, length, writer)
    check_external_links(df, length, writer)
    check_no_of_titles(df, length, writer)
    check_no_of_h1_tags(df, length, writer)
    check_canonical_urls(df, length, writer)
    check_i_b_font_tags(df, length, writer)
    check_pages_missing_page_title(df, length, writer)
    check_company_name_in_page_title(df, length, writer)
    check_duplicate_page_title(df, length, writer)
    check_duplicate_meta_desc(df, length, writer)
    check_link_to_home_page(df, length, writer)
    check_file_getting_open_in_same_tab(df, length, writer)
    check_google_meta_and_gtm(df, length, writer)
    check_page_natural_language(df, length, writer)
    check_form_controls(df, length, writer)
    check_internal_links_opening_in_new_tab(df, length, writer)
    check_delivered_by(df, length, writer)
    check_special_chars_in_path(df, length, writer)
    writer.save()
    # print(pd.ExcelFile(file).sheet_names)
    return file, filename
