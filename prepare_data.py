from bs4 import BeautifulSoup as bs


class PrepareData(object):

    def __init__(self, resp):
        self.soup = bs(resp.text, "html.parser")

    @property
    def get_all_anchor_tags(self):
        return self.soup.find_all("a")

    @property
    def get_anchor_tags_with_href(self):
        all_anchors = self.get_all_anchor_tags
        anchors = []
        for anchor in all_anchors:
            if "href" in anchor.attrs and len(anchor.attrs["href"]) >= 1 and anchor.attrs["href"] != "./" \
                    and anchor.attrs["href"] != ".#" and anchor.attrs["href"] != "" and anchor.attrs["href"] != " " \
                    and anchor.attrs["href"] != "http" and anchor.attrs["href"] != "http:":
                anchors.append(anchor)
        return anchors

    @property
    def get_img_tags(self):
        return self.soup.find_all("img")

    @property
    def get_title(self):
        return self.soup.find_all("title")

    @property
    def get_meta_desc(self):
        return self.soup.find_all("meta", attrs={"name": "description"})

    @property
    def get_og_tags(self):
        og_tags = ["og:site_name", "og:url", "og:title", "og:description", "og:image", "og:type"]
        return self.soup.find_all("meta", property=og_tags)

    @property
    def get_canonical(self):
        return self.soup.find_all("link", rel="canonical")

    @property
    def get_h_tags(self):
        headings = ["h1", "h2", "h3", "h4", "h5", "h6"]
        headings_tag = []
        heading_tags = {}
        for heading in headings:
            h_tags = self.soup.find_all(heading)
            heading_tags[heading] = h_tags
        headings_tag.append(heading_tags)
        return headings_tag

    @property
    def get_h1_tags(self):
        return self.soup.find_all("h1")

    @property
    def if_i_tags(self):
        if_i_tags = True if len(self.soup.find_all("i")) > 0 else False
        return if_i_tags

    @property
    def if_b_tags(self):
        if_b_tags = True if len(self.soup.find_all("b")) > 0 else False
        return if_b_tags

    @property
    def if_font_tags(self):
        if_font_tags = True if len(self.soup.find_all("font")) > 0 else False
        return if_font_tags

    def get_parents(self, h_tags):
        for tags in h_tags:
            parents = self.soup.find_parents(tags.name)

    @property
    def get_bg_images(self):
        import re
        divs = self.soup.find_all("div", {"style" : re.compile(r"background-image*")})
        return divs

    @property
    def get_home_page(self):
        hp_link = self.soup.find_all("a", {"href":"/"})
        return len(hp_link)

    @property
    def get_all_empty_p_tags(self):
        p_tags = self.soup.find_all("p")
        empty_p = []
        for p in p_tags:
            val = str(p.text)
            if len(val) == 0 or val == "&nbsp;":
                empty_p.append(p)
        return len(empty_p)

    @property
    def get_br_tags(self):
        count = 0
        br_tags = self.soup.find_all("br")
        for br in br_tags:
            if br.next_sibling.name == "br":
                count = count + 1
        return count

    @property
    def get_gtm_id(self):
        script_tag = self.soup.find_all("script")
        # print(script_tag)
        for tag in script_tag:
            if "src" in tag.attrs and str(tag.attrs["src"]).startswith("https://www.googletagmanager.com/gtm.js?id=GTM-"):
                print(tag)

    @property
    def get_html(self):
        html = self.soup.find_all("html")
        return html

    @property
    def skip_to_main_content(self):
        skip_to_main = self.soup.find_all("a", text=True)
        for a in skip_to_main:
            if "Skip to main content" in a.string:
                return "Present"

        return "Missing"

    @property
    def get_all_forms(self):
        forms = self.soup.find_all("form")
        return forms