####Author: Harshendra Thakur#####



To run this tool following requirements should be met:

Python 3 should be installed along with following libraries:

bs4 // pip install bs4
jira==2.0.0 // pip install jira
lxml==4.5.0 // pip install lxml
pandas==1.0.1 // pip install pandas
pdfminer==20191125 // pip install pdfminer
PyPDF2==1.26.0  // pip install PyPDF2
requests==2.23.0 // pip install requests
selenium==3.141.0 // pip install selenium
xlrd==1.2.0 // pip install xlrd
XlsxWriter==1.2.8 // pip install XlsxWriter


#################### Installation ######################
Run the .exe and proceed with the installation steps. Its pretty straight forward.

################### How to use the tool ###################

1) Run the HTReports.exe
2) In Enter URL field: enter the URL to the website you want to test
3) In Enter Company Name field: enter the Name of the company which appears in the page titles
4) In Enter Report Sender's Outlook Email field: Enter your outlook email address (i.e. your company email for example Jhon.Doe@xyz.com)
5) In Enter Outlook Password field: enter your outlook's password
6) Enter Receiver's Email Address field: enter the email address of the receiver (currently, only one email is accepted). Report will be emailed to this email address from sender's email
7) If your site is password protected, select the "Select if Site is Password Protected:" checkbox
-- Enter Username and Password in the "Enter Username" and "Enter Password" fields respectively to authenticate site login
8) If the site is having age check page the select the "Select if Age check page is there" checkbox. (Refer the heineken website)

9) Click on Run Tests button to start the test execution.

10) If you wish to log the bugs in JIRA for the bugs reported by this tool then select the "Select To Create / Update JIRA Tickets" checkbox.
- This will enable the JIRA related fields
- If this checkbox is selected "Run Tests" button will become disabled. Click on "Get JIRA fields" button.
- Once required JIRA fields are imported and displayed on the window, the "Run Tests" button will become interactive.

11) Enter JIRA URL (Note: is it mandatory to enter URL till .com only. Example "https://xyz.company.com"
12) Enter your jira username and password in the respective fields
13) Enter the project id. Like XYZA
14) Enter the username of the person to whom to assign the bugs
15) Select the values for the other required fields (if any)
16) Click on Run Tests

Note: Once the test is started, the tool appears to be Not Responding but please be patient. "Test completed" message will be displayed when the final report is generated and emailed.


You can also see the generated reports in the "reports" folder. For this you need to go to the location where you have installed HTReports and then open the reports folder and the move to the folder created with value used in "Enter Company Name" field.
There can be multiple reports, based on how many times you have ran the tests for a site, select the one which is with latest timestamp in its name.


