import re
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument


def check_file_type_and_size(all_links):
    file_type = [".pdf", ".mp3", ".mp4", ".xls", ".xlsx", ".mov", ".txt", ".zip"]
    file_size_missing = []
    file_type_missing = []
    file_open_in_same_tab = []
    file_size_as_per_investis = []
    url_result = {}
    for link in all_links:
        # print(link)
        type_present = 0
        new_url = link.attrs['href']
        phone_number = "(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{5}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})"
        if "mailto:" not in str(new_url) and not re.match('\S+@\S+', new_url) \
                and "tel:" not in str(str(new_url)) and not re.match(phone_number, str(new_url).replace(" ", "")):

            # checking file type for PDF, DOC, MOV, PPT, TXT, XLS and XLSX

            # file_type = [".pdf", ".mp3", ".mp4", ".xls", ".xlsx", ".mov", ".txt", ".zip"]
            file_types = ["pdf", "mp3", "mp4", "xls", "xlsx", "mov", "txt", "zip", "docx"]
            file_sizes = ["kb", "mb", "gb", "tb", "byte"]

            if ".pdf" in str(new_url).lower() or ".mp3" in str(new_url).lower() or \
                    ".mp4" in str(new_url).lower() or ".xls" in str(new_url).lower() or \
                    ".xlsx" in str(new_url).lower() or ".mov" in str(new_url).lower() or \
                    ".txt" in str(new_url).lower() or ".zip" in str(new_url).lower() or\
                    ".docx" in str(new_url).lower() or ".doc" in str(new_url).lower():
                if 'title' in link.attrs:
                    title = str(link.attrs["title"]).lower()

                    if "kb" in title:
                        size = re.findall(r"[-+]?\d*\.\d+|\d+", title)
                        size = ("".join(x for x in size))
                        if len(size) > 0:
                            size = int(round(float(size)))
                            if str(size) not in title:
                                file_size_as_per_investis.append(link)
                    elif "mb" in title:
                        size = re.findall(r"[-+]?\d*\.\d+|\d+", title)
                        size = ("".join(x for x in size))
                        if len(size) > 0:
                            size = float(round(float(size), 1))
                            if str(size) not in title:
                                file_size_as_per_investis.append(link)
                        # pass
                    else:
                        file_size_missing.append(link)

                if 'target' in link.attrs:
                    if link.attrs["target"] != "_blank":
                        file_open_in_same_tab.append(link)
                else:
                    file_open_in_same_tab.append(link)

                if 'title' in link.attrs:
                    title = str(link.attrs["title"]).lower()
                    for f_type in file_types:
                        if f_type in title:
                            type_present = type_present + 1
                    if type_present == 0:
                        file_type_missing.append(link)


    url_result["File size missing"] = file_size_missing if len(file_size_missing) > 0 else "None"
    url_result["Missing File Type"] = file_type_missing if len(file_type_missing) > 0 else "None"
    url_result["File getting open in same tab"] = file_open_in_same_tab if len(file_open_in_same_tab) > 0 else "None"
    url_result["File size not per Investis Standard"] = file_size_as_per_investis if len(file_size_as_per_investis) > 0 else "None"

    return url_result


def check_special_char_presence(soup):
    regex = re.compile('[@_!\'\"#$%^&*()\`+=<>?\|}{~:]')
    file_or_directory_special_char = []
    url_result = {}
    srcs = soup.find("body").find_all()
    hrefs = soup.find("body").find_all()
    for src in srcs:
        if "src" in src.attrs and "/files/" in src.attrs["src"] and "/sites/" in src.attrs["src"]:
            if regex.search(str(src.attrs["src"])) is not None:
                file_or_directory_special_char.append(str(src.attrs["src"]))
    for href in hrefs:
        if "href" in href.attrs and "/files/" in href.attrs["href"] and "/sites/" in href.attrs["href"]:
            if regex.search(str(href.attrs["href"])) is not None:
                file_or_directory_special_char.append(str(href.attrs["href"]))

    url_result["File/Directory with special character"] = file_or_directory_special_char if len(file_or_directory_special_char) > 0 else "None"
    return url_result


def check_pdf_title_presence():
    fp = open('Agm-notice-2016.pdf', 'rb')
    parser = PDFParser(fp)
    doc = PDFDocument(parser)