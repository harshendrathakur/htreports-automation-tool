import requests
import json
from requests.auth import HTTPBasicAuth
from randon_headers import jira_agents
import pandas as pd


class BugFields(object):
    bug_summary_dict = {
        "Response Code": "List of links with 404 response",
        "Page missing page title": "List of pages missing page title",
        "Company Name in page title": "List of pages without company name in page title",
        "Duplicate page title": "List of pages having duplicate page titles",
        "Duplicate meta description": "List of pages having duplicate meta description",
        "Node URLs": "List of pages with node URLs",
        "External Link Issues": "List of pages with external links missing tool-tip or getting open in same window",
        "Img tags without alt attribute": "List of pages having images with empty or without alt attribute",
        "Links without content": "List of pages with anchor tags without content",
        "Pages with i or b or font tags": "List of pages having i b font tag",
        "Missing File Type or Size": "List of pages with files missing their file size or type",
        "More than one h1 tags": "More than one h1",
        "More than one title": "List of pages with more than one h1 tag",
        "Missing Canonical URLs": "List of pages missing canonical URL",
        "Missing link to Homepage": "List of pages missing link to Homepage",
        "File getting open in same tab": "List of pages with files getting open in same tab or window",
        "Google meta and GTM code": "List of pages missing either google site verification meta tag or the GTM code",
        "Page natural lang not defined": "List of pages for which natural language is not defined",
        "Not readable by screen reader": "List of pages having form with fields not readable by screen reader",
        "Internal link open in a new tab": "List of pages having internal links which are getting open in a new window",
        "Delivered by Investis": "List of pages missing Delivered by Investis link",
        "Path or name with special char": "List of pages with Path or name with special char"

    }

    def __init__(self, url, project_key, uname, pword):
        self.url = url
        self.project_key = project_key
        self.username = uname
        self.password = pword
        self.fields = None
        self.mandatory_data_dict = None
        self.get_mandatory_fields_and_values(self.url, self.project_key, self.username,
                                                                        self.password)

    def get_mandatory_fields_and_values(self, url, project_key, username, password):
        already_present = ["summary", "issuetype", "description", "project", "priority", "customfield_10904",
                           "customfield_14005", "issuelinks", "assignee"]
        jira_url = url + '/rest/api/2/issue/createmeta?projectKeys={project_key}&issuetypeNames=Bug&expand=projects.issuetypes.fields'.format(project_key=project_key)
        username = username
        password = password
        header = {
            'User-Agent': 'xx',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'X-Atlassian-Token': 'nocheck'
        }
        required_fields_and_values = {}
        issue_types_resp = requests.get(jira_url, auth=(username, password), headers=header)
        issue_types = json.loads(issue_types_resp.text)
        self.fields = issue_types['projects'][0]['issuetypes'][0]['fields']
        required_fields = []
        for key, val in self.fields.items():
            if val['required']:
                required_fields.append(key)

        for req_fields in required_fields:
            if req_fields not in already_present:
                print(self.fields[req_fields])
                allowed_values = self.fields[req_fields]['allowedValues']
                vals = []
                for values in allowed_values:
                    if "name" in values.keys():
                        vals.append(values['name'])
                    if "value" in values.keys():
                        vals.append(values['value'])
                required_fields_and_values[req_fields] = {self.fields[req_fields]['name']: vals}
        self.mandatory_data_dict = required_fields_and_values

    def search_issues(self, jira_url, username, password):
        # print("in search_issues")
        url = jira_url
        r = requests.get(url, auth=(username, password), headers=jira_agents())

        results = r.json()
        number_of_issues = len(results['issues'])

        if number_of_issues > 0:
            issue_id = results["issues"][0]["key"]

            return issue_id
        else:
            return "create a bug or close the bug"

    def create_update_close_bug(self, jira_url, username, password, project_id, file, file_path, jira_data):
        # print("in create_update_close_bug")
        for k, v in jira_data.items():
            if self.fields[k]['schema']['type'] == 'array':
                print("arrary " + k)
                jira_data[k] = [{"name": v}]
            if self.fields[k]['schema']['type'] == 'option':
                print("option " + k)
                jira_data[k] = {"value": v}
            if self.fields[k]['schema']['type'] == 'user':
                print("assignee " + k)
                jira_data[k] = {"name": v}
            if self.fields[k]['schema']['type'] == 'string':
                print("string " + k)
                jira_data[k] = v

        bug_list = pd.ExcelFile(file).sheet_names

        for bug in self.bug_summary_dict.keys():
            if bug in bug_list:

                df = pd.read_excel(file, sheet_name=bug)
                summary = project_id + " - " + self.bug_summary_dict[bug]
                query_string = jira_url + "/rest/api/2/search?jql=project%20%3D%20{project_id}%20and%20issuetype%3Dbug%20and%20summary%20~%20%22{summary}%22%20and%20status%20not%20in%20%28%22Bug%20Closed%22%2C%20Rejected%2C%20Duplicate%2C%20Invalid%29".format(project_id=project_id, summary=summary)
                actual_result = self.bug_summary_dict[bug]
                to_do = self.search_issues(query_string, username, password)

                file_name = str(bug) + ".xlsx" if len(str(bug) + ".xlsx") <= 31 else str(bug)[:26] + ".xlsx"
                bug_file = file_path + "\\" + file_name
                with open(bug_file, "w") as f:
                    f.close()
                writer = pd.ExcelWriter(bug_file, engine="xlsxwriter")
                df.to_excel(writer, sheet_name=bug)
                writer.save()

                if to_do != "create a bug or close the bug":
                    self.add_comment(bug_id=to_do, jira_url=jira_url, username=username, password=password, file=file_name)
                    self.upload_attachment(bug_id=to_do, jira_url=jira_url, username=username, password=password, file=bug_file)
                else:
                    self.log_bugs(jira_url, project_id, username, password, summary, actual_result, jira_data, bug_file)
            else:
                close_summary = project_id + " - " + self.bug_summary_dict[bug]
                close_query_string = jira_url + "/rest/api/2/search?jql=project%20%3D%20{project_id}%20and%20issuetype%3Dbug%20and%20summary%20~%20%22{summary}%22%20and%20status%20not%20in%20%28%22Bug%20Closed%22%2C%20Rejected%2C%20Duplicate%2C%20Invalid%29".format(
                    project_id=project_id, summary=close_summary)
                close_to_do = self.search_issues(close_query_string, username, password)
                if close_to_do == "create a bug or close the bug":
                    pass

    def log_bugs(self, url, project_key, uname, pword, summary, actual_result, jira_data, bug_file):

        # here need to identify the type of the field and based on that need to assign its value
        header = {
                    "User-Agent": "xx",
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                      }
        auth = HTTPBasicAuth(uname, pword)
        j_url = url + "/rest/api/2/issue/"
        fields_dict = {
                "project": {"key": project_key},
                "summary": summary,
                "description": "Latest report has been attached, please check latest file",
                "issuetype": {"name": "Bug"},
                "customfield_14005": actual_result,
                "customfield_10904": "This should be fixed"
            }
        fields_dict.update(jira_data)

        data = {"fields": fields_dict}

        data = json.dumps(data)
        cb = requests.post(j_url, data=data, headers=header, auth=auth)

        if cb.status_code == 201:
            self.upload_attachment(json.loads(cb.text)["key"], url, uname, pword, bug_file)

    def upload_attachment(self, bug_id, jira_url, username, password, file):

        url = jira_url + '/rest/api/2/issue/{id}/attachments'.format(id=bug_id)
        headers = {"X-Atlassian-Token": "nocheck"}
        files = {'file': open(file, 'rb')}

        r = requests.post(url, auth=(username, password), files=files, headers=jira_agents())

    def add_comment(self, bug_id, jira_url, username, password, file):

        url = jira_url + '/rest/api/2/issue/{id}/comment'.format(id=bug_id)

        comment = "Latest report has been attached, please check latest file [^{file}]".format(file=file)

        data = {"body": comment}
        data = json.dumps(data)
        header = {
            'User-Agent': 'xx',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'X-Atlassian-Token': 'nocheck', 'Content-Type': 'application/json'
        }
        r = requests.post(url, auth=(username, password), data=data, headers=header)

