import requests
from get_url import GetResponse
from urllib.parse import urlparse
from bs4 import BeautifulSoup as bs
from randon_headers import random_headers


def check_site_map(url, auth_username, auth_password, host_scheme):
    get_resp = GetResponse()
    parsed = urlparse(url)
    host = parsed.scheme + "://" + parsed.netloc
    sitemap_xml_url = host + "/sitemap.xml"
    robot_txt_url = host + "/robots.txt"
    sitemap = {}
    resp = get_resp.get_url(sitemap_xml_url, auth_username, auth_password, host_scheme, None)
    soup = bs(resp.text, "lxml")
    locs = soup.find_all("loc")
    node_urls = []
    # diff_scheme = []
    for loc in locs:
        if "/node" in str(loc):
            node_urls.append(str(loc)[5:-6])
    sitemap["Node URLs"] = node_urls if len(node_urls) > 0 else "There are no Node URLs in the sitemap.xml"

    return sitemap


def check_privacy_presence(footer, parsed):
    links = footer.find_all("a")
    privacy_count = 0
    accessibility_count = 0
    privacy = parsed.scheme + "://" + parsed.netloc + "/privacy"
    accessibility = parsed.scheme + "://" + parsed.netloc + "/accessibility"
    url_result = {}
    for a in links:
        if "href" in a.attrs:
            if str(a.attrs["href"]).lower().startswith("/privacy") or str(a.attrs["href"]).lower().startswith(privacy):
                # print(a)
                url_result["Privacy Statement"] = "Present"
                privacy_count = privacy_count + 1

            if str(a.attrs["href"]).lower().startswith("/accessibility") or str(a.attrs["href"]).lower().startswith(accessibility):
                # print(a)
                url_result["Accessibility Statement"] = "Present"
                accessibility_count = accessibility_count + 1

    if accessibility_count == 0:
        url_result["Accessibility Statement"] = "Missing"
    if privacy_count == 0:
        url_result["Privacy Statement"] = "Missing"
    return url_result


def check_sitemap_html_presence(footer, parsed):
    sitemap_one = parsed.scheme + "://" + parsed.netloc + "/sitemap"
    sitemap_two = parsed.scheme + "://" + parsed.netloc + "/site-map"
    sitemap = 0
    links = footer.find_all("a", href=True)
    url_result = {}
    for link in links:
        if str(link.attrs["href"]).lower().startswith(sitemap_one) or str(link.attrs["href"]).lower().startswith("/sitemap") \
            or str(link.attrs["href"]).lower().startswith(sitemap_two) or str(link.attrs["href"]).lower().startswith("/site_map"):
            sitemap = sitemap + 1
    if sitemap > 0:
        url_result["Sitemap HTML"] = "Sitemap html is present"
    else:
        url_result["Sitemap HTML"] = "Missing"

    return url_result


def check_xhtml_errors(url, driver, validate):

    error_result = []
    error_blocks = driver.find_elements_by_xpath("//li[@class='error']")
    error_messages = driver.find_elements_by_xpath("//li[@class='error']/p[1]/span")
    error_from_to_line = driver.find_elements_by_xpath("//li[@class='error']/p[2]")
    error_code = driver.find_elements_by_xpath("//li[@class='error']/p[@class='extract']")

    x = 0
    while x < len(error_blocks):
        url_result = {}
        url_result["Error Page URL"] = url
        url_result["Error Reference URL"] = validate
        url_result["Error message"] = error_messages[x].text
        url_result["Error From line to line"] = error_from_to_line[x].text
        url_result["Error Code"] = error_code[x].text
        x = x + 1
        error_result.append(url_result)
    return error_result


def validate_warning_xhtml(driver, url, validate):
    x = 0
    warnings_result = []
    warnings_blocks = driver.find_elements_by_xpath("//li[@class='info warning']")
    warning_messages = driver.find_elements_by_xpath("//li[@class='info warning']/p[1]/span")
    warning_from_to_line = driver.find_elements_by_xpath("//li[@class='info warning']/p[2]")
    warning_code = driver.find_elements_by_xpath("//li[@class='info warning']/p[@class='extract']")
    while x < len(warnings_blocks):
        url_result = {}
        url_result["Warning Page URL"] = url
        url_result["Warning Reference URL"] = validate
        url_result["Warning message"] = warning_messages[x].text
        url_result["Warning From line to line"] = warning_from_to_line[x].text
        url_result["Warning Code"] = warning_code[x].text
        x = x + 1
        warnings_result.append(url_result)
    return warnings_result


def check_css_errors(url, driver, validate):
    error_result = []


def check_css_validation(url, soup, checked_css):
    all_css = soup.find_all("link")
    invalid_css = []
    already_checked = []
    css_result = []
    for css in all_css:

        # error = 0
        if 'rel' in css.attrs and 'stylesheet' in str(css.attrs['rel']) and 'href' in css.attrs:
            if str(css.attrs['href']).startswith("/sites/") and "/files/" in str(css.attrs['href']):
                if css.attrs['href'] not in checked_css:
                    checked_css.append(css.attrs['href'])
                    parsed = urlparse(url)
                    validate_css = parsed.scheme + "://" + parsed.netloc + css.attrs['href']
                    # print("Checking css file: " + str(validate_css))
                    validate = "https://jigsaw.w3.org/css-validator/validator?uri={validate_css}&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en".format(
                        validate_css=validate_css)
                    css_resp = requests.get(validate, headers=random_headers())
                    css_soup = bs(css_resp.text, "html.parser")

                    errors_blocks = css_soup.find_all("div", {'class': 'error-section'})

                    if len(errors_blocks) > 0:
                        for errors in errors_blocks:

                            # error_row = errors.find_all("tr", {'class':'error'})
                            error_row = errors.find_all("tr", class_='error')
                            uri = errors.find("h4").find("a").text
                            for error in error_row:
                                url_result = {}
                                columns = error.findChildren("td")
                                code_context = columns[1].string
                                parsed_error = columns[2].text

                                url_result["URI"] = uri
                                url_result["Code Context"] = code_context
                                url_result["Parse Error"] = parsed_error
                                css_result.append(url_result)
                                # print(url_result)
    return css_result