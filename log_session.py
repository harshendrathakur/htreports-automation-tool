from bs4 import BeautifulSoup as bs
import requests
import time
from randon_headers import random_headers


def log_session(driver, url):
    # driver.get(url)
    time.sleep(3)
    # driver.find_element_by_xpath("//button[@id='ensBtnYes']").click()
    driver.find_element_by_xpath("//input[@id='edit-dob-day']").send_keys("12")
    driver.find_element_by_xpath("//input[@id='edit-dob-month']").send_keys("07")
    driver.find_element_by_xpath("//input[@id='edit-dob-year']").send_keys("1988")
    driver.find_element_by_xpath("//button[@id='edit-submit-button']").click()
    time.sleep(3)

    all_cookies = driver.get_cookies()

    data = {

            "dob_day": "12",
            "dob_month": "07",
            "dob_year": "1988",
            "country": "IN",
            "form_id": "age_gate_thc_user_form",
            "op": "Enter"
        }

    with requests.session() as s:
        r = s.get(url, headers=random_headers())
        for cookie in all_cookies:
            if cookie["name"] == "age_gate_access":
                s.cookies["age_gate_access"] = cookie["value"]
            if cookie["name"].startswith("SSESS"):
                s.cookies[cookie["name"]] = cookie["value"]
        soup = bs(r.text, "html.parser")
        data["form_build_id"] = soup.find("input", attrs={'name': 'form_build_id'})["value"]

        r = s.post(url, data=data, headers=random_headers())
    return s