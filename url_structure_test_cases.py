from urllib import parse


def check_google_site_verification(soup):
    url_result = {}
    # print("within check_google_site_verification")
    google_site_verification = soup.find_all("meta")
    # print("number of metas - " + str(len(google_site_verification)))
    gsv = 0
    for meta in google_site_verification:
        if "name" in meta.attrs and "google-site-verification" in str(meta.attrs["name"]):
            url_result["Google Site Verification Meta Tag"] = meta.attrs["content"]
            gsv = gsv + 1
    if gsv == 0:
        url_result["Google Site Verification Meta Tag"] = "Missing on this page"
    return url_result


def check_google_analytics_code(soup):
    # print("within check_google_analytics_code")
    all_scripts = soup.find_all("script")
    src_scripts = soup.find_all("script", src=True)
    gtm_code = None
    all_gtm = 0
    url_result = {}

    for src in src_scripts:
        if "https://www.google-analytics.com/analytics.js" == src.attrs["src"]:
            analytics_script = src
            # print(analytics_script)
            all_gtm = all_gtm + 1

        if "https://www.googletagmanager.com/gtm.js?id=GTM-" in str(src.attrs["src"]):
            tag_manager_script = src
            tag_mg_src = src.attrs['src']
            # print(tag_mg_src)
            parsed = dict(parse.parse_qsl(parse.urlsplit(tag_mg_src).query))
            gtm_code = parsed["id"]
            # print("GTM code is: " + str(gtm_code))
            if gtm_code is not None and len(str(gtm_code)) > 0:
                all_gtm = all_gtm + 1

    for script in all_scripts:
        if "https://www.googletagmanager.com/gtm.js?id=" in str(script) and gtm_code is not None and gtm_code in str(script):
                # print(script)
                all_gtm = all_gtm + 1

    if all_gtm >= 3:
        url_result["GTM code is present"] = "Yes, "+ str(gtm_code)
    else:
        url_result["GTM code is present"] = "No"

    return url_result
