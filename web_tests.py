from bs4.element import Comment
from prepare_data import PrepareData
from test_cases import *
from url_structure_test_cases import *
from rich_media_test_cases import *
from global_test_cases import *
import time


class WebTestsSes(object):

    final_result = []
    final_http_status_result = []
    external_url_result = []
    url_list = []
    external_url_list = []
    error_result = []
    warning_result = []
    tags_not_count = ["script", "link", "meta", "head", "style", "title", "noscript", "None"]

    headings = ["h1", "h2", "h3", "h4", "h5", "h6"]
    file_type = [".pdf", ".mp3", ".mp4", ".xls", ".xlsx", ".mov", ".txt", ".zip"]
    negative_resp = ["SSL Error", "Connection Error", "Connect Timeout", "Missing Schema error", "Invalid URL error",
                     "BaseHTTP error", "Timeout", "Read Timeout error", "Not accessible"]
    urls_on_page = []
    unique_desc = []
    unique_title = []

    def tag_visible(self, element):
        if element.parent.name in ['style', 'script', 'head', 'html', 'title', '[document]']:
            return False
        if isinstance(element, Comment):
            return False
        return True

    def find_all_urls_ses(self, resp, base_url, url_list, index, company_name, base_host_name, base_host_scheme,
                          ses, driver, checked_css):


        delivered_by_investis = 0
        url_result = {'URL': url_list[index]}

        try:
            # print("check_resp_code")
            resp_status = check_resp_code(resp)
            url_result.update(resp_status)
        except:
            # print("Issue in check_resp_code")
            pass
        if ".pdf" in url_list[index]:
            try:
                # print("checking PDF document title")
                url_result.update(check_pdf_title(resp.url))
            except:
                pass
        else:
            url_result.update({"PDF Document Title": "Not applicable"})

        if ".pdf" not in str(url_list[index]).lower() and ".mp3" not in str(url_list[index]).lower() and \
                ".mp4" not in str(url_list[index]).lower() and ".xls" not in str(url_list[index]).lower() and \
                ".xlsx" not in str(url_list[index]).lower() and ".mov" not in str(url_list[index]).lower() and \
                ".txt" not in str(url_list[index]).lower() and ".zip" not in str(url_list[index]).lower() and \
                ".wav" not in str(url_list[index]).lower() and ".doc" not in str(url_list[index]).lower() and \
                ".docx" not in str(url_list[index]).lower():

            soup = PrepareData(resp)  # fetching the DOM using python requests
            img = soup.get_img_tags  # fetching all the img tags
            all_anchors = soup.get_all_anchor_tags  # fetching all the anchor tags
            anchor_href = soup.get_anchor_tags_with_href  # fetching all the anchor tags having href
            titles = soup.get_title  # fetching all the title tags
            desc = soup.get_meta_desc  # fetching all the meta description tags
            og_tags = soup.get_og_tags  # fetching all the og meta tags
            h1_tags = soup.get_h1_tags  # fetching all the h1 tags
            h_tags = soup.get_h_tags  # fetching list of dictionary of headings h1 till h6
            canonical = soup.get_canonical  # fetching canonical url
            i_tags = soup.if_i_tags  # fetching i tags
            b_tags = soup.if_b_tags  # fetching b tags
            font_tags = soup.if_font_tags  # fetching font tags

            sele_soup = bs(driver.page_source, "html.parser")  # fetching dom using selenium

            # "Performing XHTML validation"
            # try:
            #     print("Performing XHTML validation")
            #     parsed = urlparse(resp.url)
            #     scheme = parsed.scheme
            #     url_to_check = resp.url[(len(scheme) + 3):]
            #     validate = "https://validator.w3.org/nu/?doc={scheme}%3A%2F%2F{url_to_check}".format(scheme=scheme,
            #                                                                                          url_to_check=url_to_check)
            #     driver.get(validate)
            #     time.sleep(3)
            #     self.error_result.extend(check_xhtml_errors(resp.url, driver, validate))
            #     self.warning_result.extend(validate_warning_xhtml(driver, resp.url, validate))
            # except:
            #     pass

            # try:
            #     url_result.update(check_css_validation(url_list[index], sele_soup, driver, checked_css))
            # except:
            #     pass

            try:
                # print("check_image_tags")
                image_tags = check_image_tags(img)
                url_result.update(image_tags)
            except:
                # print("Issue in check_image_tags")
                pass

            try:
                # print("check_page_title")
                page_title = check_page_title(titles, company_name, soup)
                url_result.update(page_title)
            except:
                # print("Issue in check_page_title")
                pass
            try:
                # print("check_page_desc")
                page_desc = check_page_desc(desc, company_name)
                url_result.update(page_desc)
            except:
                # print("Issue in check_page_desc")
                pass
            try:
                # print("check_h1_tags")
                h1_tags = check_h1_tags(h1_tags)
                url_result.update(h1_tags)
            except:
                # print("Issue in check_h1_tags")
                pass
            try:
                # print("check_i_b_font_tags")
                i_b_font_tags = check_i_b_font_tags(i_tags, b_tags, font_tags)
                url_result.update(i_b_font_tags)
            except:
                # print("Issue in check_i_b_font_tags")
                pass
            try:
                # print("check_og_tags")
                og_tags = check_og_tags(og_tags)
                url_result.update(og_tags)
            except:
                # print("Issue in check_og_tags")
                pass
            try:
                # print("check_canonical_url")
                canonical_url = check_canonical_url(canonical)
                url_result.update(canonical_url)
            except:
                # print("Issue in check_canonical_url")
                pass
            try:
                # print("check_h_tag_presence")
                h_tags = check_h_tag_presence(h_tags)
                url_result.update(h_tags)
            except:
                # print("Issue in check_h_tag_presence")
                pass
            try:
                # print("check_image_size")
                bg_images = soup.get_bg_images
                larger_images = check_image_size(img, bg_images, url_list[index])
                url_result.update(larger_images)
            except:
                # print("Issue in check_image_tags")
                pass
            try:  ##########################################
                if str(base_url) != str(resp.url):
                    hp_links = soup.get_home_page
                    if hp_links > 0:
                        url_result["Home page link is present"] = "Present"
                    else:
                        url_result["Home page link is present"] = "Either missing or alias not set as '/'"
                else:
                    url_result["Home page link is present"] = "Not required for homepage"
            except:
                # print("Issue in Home page link")
                pass
            try:
                # print("checking empty p")
                empty_p = soup.get_all_empty_p_tags
                if empty_p > 0:
                    url_result["Empty p tags are"] = "Present"
                else:
                    url_result["Empty p tags are"] = "Not Present"
            except:
                # print("Issue in empty p")
                pass
            try:
                # print("checking br tags")
                br_tags = soup.get_br_tags
                if br_tags > 0:
                    url_result["Consecutive br tags"] = "Present"
                else:
                    url_result["Consecutive br tags"] = "Not Present"
            except:
                # print("Issue in br tags")
                pass
            try:
                # print("cheking links without content")
                link_wo_content = check_link_without_content(all_anchors)
                url_result.update(link_wo_content)
            except:
                # print("Issue in links without content")
                pass
            try:
                # print("checking file type and size and open in new tab")
                file_type_and_size = check_file_type_and_size(anchor_href)
                url_result.update(file_type_and_size)
            except:
                # print("Issue in file type and size")
                pass

            try:
                # print("checking node urls")
                node_urls = check_node_urls(anchor_href)
                url_result.update(node_urls)
            except:
                # print("Issue in node urls")
                pass

            try:
                # print("checking google_analytics_code")
                analytic_script = check_google_analytics_code(sele_soup)
                # print(analytic_script)
                url_result.update(analytic_script)
            except:
                # print("Issue in google_analytics_code")
                pass

            try:
                # print("check duplicate meta desc")
                duplicate_descriptions = check_duplicate_descriptions(desc, self.unique_desc)
                url_result.update(duplicate_descriptions)
            except:
                # print("Issue in duplicate meta desc")
                pass

            try:
                # print("check duplicate page titles")
                duplicate_titles = check_duplicate_page_title(titles, self.unique_title)
                url_result.update(duplicate_titles)
            except:
                # print("Issue in duplicate page titles")
                pass

            try:
                # print("check if language is mentioned or not")
                html = soup.get_html
                html_lang = check_natural_language(html[0])
                url_result.update(html_lang)
            except:
                # print("Issue in language is mentioned or not")
                pass
            try:
                # print("checking Skip to main content")
                if soup.skip_to_main_content == "Present":
                    url_result["Skip to main content"] = "Present"
                else:
                    url_result["Skip to main content"] = "Missing"
            except:
                # print("Issue in Skip to main contentt")
                pass
            try:
                google_site_verification = check_google_site_verification(sele_soup)
                url_result.update(google_site_verification)
            except:
                # print("Issue in google site verification code existence")
                pass

            # check if form controls are read able by screen reader or not
            try:
                # print("checking forms")
                url_result.update(check_form_labels(soup.get_all_forms, soup.soup))
            except:
                # print("Issue in forms")
                pass
            # check if in-line style is applied or if style tags are present on a page
            try:
                # print("checking in-line style")
                url_result.update(check_inline_style(soup.soup))
            except:
                # print("Issue in in-line style")
                pass
            try:
                # print("checking space before a </p> tag")
                url_result.update(check_space_before_closing_p(soup.soup))
            except:
                # print("Issue in space before a </p> tag")
                pass
            try:
                # print("checking internal links getting open in a new tab")
                url_result.update(check_internal_links_opening_in_new_window(anchor_href, resp.url))
            except:
                # print("Issue in internal links getting open in a new tab")
                pass

            if ("share" in resp.url and "price" in resp.url) or ("share" in resp.url and "-chart" in resp.url):
                try:
                    # print("checking share price chart tables")
                    url_result.update(check_share_price_chart_disclaimer(sele_soup, driver))
                except:
                    pass
            else:
                url_result["Disclaimer is present"] = "Not applicable"

            try:
                # print("checking For data tables, identify row and column headers.")
                tables = sele_soup.find_all("table")
                if len(tables) > 0:
                    url_result.update(check_table_headers(tables))
                else:
                    url_result["Row and column headers and role are missing"] = "Not applicable"
            except:
                pass

            try:
                # print("checking For data tables, identify row and column headers in iFrames")
                iframes = sele_soup.find_all("iframe")
                if len(iframes) > 0:
                    url_result.update(check_tables_in_iframes(iframes, driver))
            except:
                pass

            try:
                # print("checking special characters")
                url_result.update(check_special_char_presence(soup.soup))
            except:
                pass
            try:
                url_result.update(check_h_tag_hierarchy(soup.soup))
            except:
                pass
            host_name = base_host_name
            host_scheme = base_host_scheme
            is_auth = False
            req_url = url_list[index]
            self.url_list = url_list
            node_urls = []
            ext_links_wo_target = []
            ext_links_empty_tgt = []
            ext_links_wo_title = []
            ext_links_empty_title = []
            ext_links_wo_msg = []
            links_without_content = []

            diff_schema = []
            back_to_top = False
            urls_on_page_dict = {}
            urls_on_this_page = []
            meaningless_phrases = []
            formed_url = req_url

            if resp not in self.negative_resp:
                if ".pdf" not in str(url_list[index]).lower() and ".mp3" not in str(url_list[index]).lower() and \
                        ".mp4" not in str(url_list[index]).lower() and ".xls" not in str(url_list[index]).lower() and \
                        ".xlsx" not in str(url_list[index]).lower() and ".mov" not in str(url_list[index]).lower() and \
                        ".txt" not in str(url_list[index]).lower() and ".zip" not in str(url_list[index]).lower():

                    soup = bs(resp.text, 'html.parser')

                    # collect all anchor tags
                    all_links = soup.find_all('a')

                    for link in all_links:
                        external_url_status = {}
                        # checking back to top
                        if ('id' in link.attrs and 'title' in link.attrs) and \
                                (link.attrs['id'].lower() == "backtotop" or "back to top" in link.attrs['title'].lower
                                 or "scroll to top" in link.attrs['title'].lower):
                            back_to_top = True

                        if "href" in link.attrs and len(link.attrs["href"]) > 1 and link.attrs["href"] != "./" \
                                and link.attrs["href"] != ".#" and link.attrs["href"] != "" and link.attrs[
                            "href"] != " " \
                                and link.attrs["href"] != "http" \
                                and link.attrs["href"] != "http:":

                            new_url = link.attrs['href']
                            # Do not use meaningless phrases such as "click here" or "more" as entire link text.
                            if str(link.string).lower() in ["click here", "more", "read more"]:
                                meaningless_phrases.append(link)

                            phone_number = "(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{5}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})"
                            if "mailto:" not in str(new_url) and not re.match('\S+@\S+', new_url) \
                                    and "tel:" not in str(str(new_url)) and not re.match(phone_number,
                                                                                         str(new_url).replace(" ", "")):

                                parsed = urlparse(new_url)

                                if len(parsed.netloc) > 0 and parsed.netloc != host_name:
                                    formed_url = url_list[index]
                                    if "target" not in link.attrs:
                                        ext_links_wo_target.append(new_url)
                                    elif link.attrs['target'] != "_blank":
                                        ext_links_empty_tgt.append(new_url)
                                    if "title" not in link.attrs:
                                        ext_links_wo_title.append(new_url)
                                    elif "new window" not in str(link.attrs['title']).lower():
                                        ext_links_wo_msg.append(new_url)
                                    elif len(link.attrs) == 0:
                                        ext_links_empty_title.append(new_url)
                                elif parsed.scheme == "http" or parsed.scheme == "https":
                                    formed_url = new_url

                                    # checking for URLs with different schema
                                    if host_scheme != parsed.scheme:
                                        diff_schema.append(formed_url)

                                elif not new_url.startswith("http"):

                                    if not new_url.startswith("/"):

                                        formed_url = url_list[index][::-1]

                                        formed_url = formed_url[formed_url.index("/"):]

                                        formed_url = formed_url[::-1]

                                        formed_url = formed_url + new_url

                                    else:

                                        formed_url = host_scheme + "://" + host_name + new_url

                                if r"/node/" in formed_url:
                                    url_result['Node Urls on page'] = "Present"
                                    node_urls.append(formed_url)
                                else:
                                    url_result['Node Urls on page'] = "No node URLs on this page"

                                self.url_list.append(str(formed_url))
                                ###############################################################
                                urls_on_this_page.append(str(formed_url))
                                ###############################################################

                                if len(external_url_status) > 0:
                                    self.external_url_result.append(external_url_status)

                                self.url_list = list(dict.fromkeys(self.url_list))
                                if link.attrs["href"] == "https://www.investisdigital.com/":
                                    delivered_by_investis = 1

                    url_result["Delivered by Investis"] = "Present" if delivered_by_investis == 1 else "Missing"
                    urls_on_page_dict["URL"] = url_list[index]
                    urls_on_page_dict["URLs on this page"] = urls_on_this_page
                    self.urls_on_page.append(urls_on_page_dict)
                    ext_links_in_same_window = ext_links_wo_target + ext_links_empty_tgt
                    url_result["External links open in same window"] = ext_links_in_same_window if len(
                        ext_links_in_same_window) > 0 else "None"
                    ext_links_wo_new_win_msg = ext_links_empty_title + ext_links_wo_msg + ext_links_wo_title
                    url_result["External link without new window message"] = ext_links_wo_new_win_msg if len(
                        ext_links_wo_new_win_msg) > 0 else "None"
                    url_result["Links without content"] = links_without_content if len(
                        links_without_content) > 0 else "None"
                    url_result["Meaningless Phrases"] = meaningless_phrases if len(meaningless_phrases) > 0 else "None"
                    if back_to_top:
                        url_result["Back to top"] = "Present"
                    else:
                        url_result["Back to top"] = "Back to top is missing"

                    self.final_result.append(url_result)
        else:
            url_result["List of Node urls on page"] = "It is a file"
            url_result["External links open in same window"] = "It is a file"
            url_result["External link without new window message"] = "It is a file"
            url_result["Links without content"] = "It is a file"
            url_result["Img tags without alt attribute"] = "It is a file"
            url_result["Img tags with empty or none alt text"] = "It is a file"
            url_result["Missing File Type"] = "It is a file"
            url_result["File size missing"] = "It is a file"
            url_result["Img with alt LOGO"] = "It is a file"
            url_result["Back to top"] = "It is a file"
            url_result["Meaningless Phrases"] = "It is a file"
            url_result["Image with size greater than 50KB"] = "It is a file"
            url_result["Has headings"] = "It is a file"
            url_result["Home page link is present"] = "It is a file"
            url_result["Empty p tags are"] = "It is a file"
            url_result["Consecutive br tags"] = "It is a file"
            url_result["if b tag present"] = "It is a file"
            url_result["if font tag present"] = "It is a file"
            url_result["if i tag present"] = "It is a file"
            url_result["File getting open in same tab"] = "It is a file"
            url_result["Duplicate meta description on this page"] = "It is a file"
            url_result["Duplicate title on this page"] = "It is a file"
            url_result["Privacy Statement"] = "It is a file"
            url_result["Accessibility Statement"] = "It is a file"
            url_result["Natural language"] = "It is a file"
            url_result["Skip to main content"] = "It is a file"
            url_result["Form controls not readable by screen reader"] = "It is a file"
            url_result["Heading longer than 10 words in length"] = "It is a file"
            url_result["Space before a </p> tag"] = "It is a file"
            url_result["Paragraph longer than 100 words"] = "It is a file"
            url_result["Internal links getting open in a new window"] = "It is a file"
            url_result["Meta title"] = "It is a file"
            url_result["Company Name in page title"] = "It is a file"
            url_result["Duplicate title on this page"] = "It is a file"
            url_result["Meta description"] = "It is a file"
            url_result["Duplicate meta description on this page"] = "It is a file"
            url_result["Company Name in meta description"] = "It is a file"
            url_result["og tags"] = "It is a file"
            url_result["Number of h1 on page"] = "It is a file"
            url_result["More than one title"] = "It is a file"
            url_result["Canonical URL"] = "It is a file"
            url_result["Google Site Verification Meta Tag"] = "It is a file"
            url_result["GTM code is present"] = "It is a file"
            url_result["Tags containing in-line style"] = "It is a file"
            url_result["Style tags present on page"] = "It is a file"
            url_result["File size not per Investis Standard"] = "It is a file"
            url_result["Disclaimer is present"] = "It is a file"
            url_result["Row and column headers and role are missing"] = "It is a file"
            url_result["File/Directory with special character"] = "It is a file"
            url_result["Img with lower case initial"] = "It is a file"
            url_result["H tags hierarchy"] = "It is a file"

            self.final_result.append(url_result)
        return self.url_list
