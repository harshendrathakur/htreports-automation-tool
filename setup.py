import cx_Freeze
from cx_Freeze import *
import sys
import os

base = None

if sys.platform == 'win32':
    base = "Win32GUI"

cwd = os.getcwd()
file_path = cwd + "/reports"
executables = [cx_Freeze.Executable(r"C:\Users\harshendra.thakur\Music\modular\HtReports\HTReports.py", base=base, icon="HTReports.ico")]

cx_Freeze.setup(
    name="HT Reports",
    options={
        "build_exe": {
            "packages": ["tkinter", "pandas", "bs4", "jira", "lxml", "pdfminer", "PyPDF2", "requests", "selenium"
                , "xlrd", "XlsxWriter", "email", "json", "urllib"],
            "include_files": ["HTReports.ico", r"C:\Users\harshendra.thakur\Music\modular\HtReports",
                              r"C:\Users\harshendra.thakur\Music\modular\phantomjs-2.1.1-windows",
                              r"C:\Users\harshendra.thakur\Music\modular\driver", "Readme.txt"]}},
    version="0.01",
    description="Make your website better",
    executables=executables,
    author="Harshendra Thakur"
)
