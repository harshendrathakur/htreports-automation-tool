import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import os


# def send_emails(file_to_send, from_address, toaddr, password, company_name):
def send_emails(file_path, from_address, toaddr, password, company_name):
    sender = from_address[:from_address.index(".")].capitalize()

    # string to store the body of the mail
    body = """
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
        <title>QA Automation Tool - HTReports</title>
        <p style="background-color: #002366;" class='header'><img src='investis_logo_2.png'/></p>
        <p>Hi,</p>
        <p>PFA the latest 'QA Automation Tool - HTReports' for {site}:</p>

        <p><strong>The attached consolidated report</strong> contains result for all the tests which were ran.</p>
        <p><strong>The Bug report </strong> contains the identified bugs</p>

        <p style='line-height:1em;'>Regards</p>
        <p style='line-height:1em;'>{sender}</p>
        
        <footer><p style='line-height:1em; font-size:9px; color:#1d1d1d;'>The contents of this email including its 
        attachments (if any) are confidential and may be privileged. If you are not the intended recipient, you should 
        not copy it or use it for any purpose nor disclose its contents to any other person. Please notify the person 
        named above if this email has been sent to you in error</p></footer>
        """.format(sender=sender, site=company_name)


    fromaddr = from_address

    # instance of MIMEMultipart
    msg = MIMEMultipart()

    # storing the senders email address
    msg['From'] = fromaddr

    # storing the receivers email address
    # msg['To'] = toaddr
    recipients = toaddr.split(",")
    msg['To'] = ", ".join(recipients)

    # storing the subject
    msg['Subject'] = "QA Automation Tool - HTReports - {site}".format(site=company_name)

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'html'))

    # open the file to be sent
    filename = "Bug_report.xlsx"
    # file_path = file_to_send
    print("within send email")
    print("File path is: " + file_path)
    files = os.listdir(file_path)
    print(files)
    for file in files:
        if "_copy.xlsx" not in file:
            print("attaching: " + file)
            attach_file = file_path + "\\" + file
            attachment = open(attach_file, "rb")

            # instance of MIMEBase and named as p
            p = MIMEBase('application', 'octet-stream')

            # To change the payload into encoded form
            p.set_payload(attachment.read())

            # encode into base64
            encoders.encode_base64(p)

            p.add_header('Content-Disposition', "attachment; filename= %s" % os.path.basename(file))

            # attach the instance 'p' to instance 'msg'
            msg.attach(p)


    print("now sending email")
    # creates SMTP session
    s = smtplib.SMTP(host='smtp-mail.outlook.com', port=587)
    s.ehlo()
    # start TLS for security
    s.starttls()
    s.ehlo()
    # Authentication
    s.login(fromaddr, password)

    # Converts the Multipart msg into a string
    text = msg.as_string()

    # sending the mail
    s.sendmail(fromaddr, toaddr, text)
    print("now sending email 2")
    # terminating the session
    s.quit()