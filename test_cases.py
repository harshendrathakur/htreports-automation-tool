from trim_tags import trim_tags
import urllib.request
from urllib.parse import urlparse
from bs4 import BeautifulSoup as bs
import io
from PyPDF2 import PdfFileReader
import requests
from randon_headers import random_headers


def check_page_title(titles, company_name, soup):
    url_result = {}
    if_title = True if len(titles) > 0 else False
    if if_title:
        title = titles[0]
        url_result['Meta title'] = title.text
        if "| " + company_name.lower() not in title.text.lower() and "- " + company_name.lower() not in title.text.lower():
            url_result["Company Name in page title"] = "Missing"
        else:
            url_result["Company Name in page title"] = "Present"
    else:
        url_result['Meta title'] = "Page title is missing"
    if len(titles) > 1:
        url_result['More than one title'] = "True"
    else:
        url_result['More than one title'] = "False"
    return url_result


#  THIS IS A DEFAULT SITE LEVEL META DESCRIPTION 320 320 #### The default description
def check_page_desc(meta_desc, company_name):
    url_result = {}
    if_meta_desc = True if len(meta_desc) > 0 else False
    if if_meta_desc:
        if 'content' in meta_desc[0].attrs:
            if len(meta_desc[0].attrs['content']) > 0 and meta_desc[0].attrs['content'] != "":
                url_result['Meta description'] = meta_desc[0].attrs['content']
                if "THIS IS A DEFAULT SITE LEVEL META DESCRIPTION 320 320" in meta_desc[0].attrs['content']:
                    url_result['Meta description'] = "Default meta description on page"
                if company_name.lower() not in meta_desc[0].attrs['content'].lower():
                    url_result["Company Name in meta description"] = "Missing"
                else:
                    url_result["Company Name in meta description"] = "Present"
            else:
                url_result['Meta description'] = "Meta description is missing"
        else:
            url_result['Meta description'] = "Meta description is missing"
    else:
        url_result['Meta description'] = "Meta description is missing"
    return url_result


def check_h1_tags(h_tags):
    url_result = {}
    url_result["Number of h1 on page"] = len(h_tags)
    return url_result


def check_i_b_font_tags(i_tags, b_tags, font_tags):
    url_result = {}
    url_result['if i tag present'] = i_tags
    url_result['if b tag present'] = b_tags
    url_result['if font tag present'] = font_tags
    return url_result


def check_og_tags(og_tags):
    url_result = {}
    if len(og_tags) == 0:
        url_result["og tags"] = "Missing og tag"
    else:
        url_result["og tags"] = "Present"
    return url_result


def check_canonical_url(canonical):
    url_result = {}
    if len(canonical) > 0:
        url_result["Canonical URL"] = "Present"
    else:
        url_result["Canonical URL"] = "Missing canonical url"
    return url_result


def check_image_tags(img_tags):
    url_result = {}
    img_tags_wo_alt_attr = []
    img_tags_with_empty_alt_txt = []
    img_with_alt_logo = []
    img_alt_starts_lower = []
    for tags in img_tags:
        if "alt" not in tags.attrs:
            img_tags_wo_alt_attr.append(trim_tags(tags))
        elif len(tags.attrs['alt']) == 0 or tags.attrs['alt'] is None:
            img_tags_with_empty_alt_txt.append(trim_tags(tags))
        elif len(tags.attrs["alt"]) > 0:
            if "logo" in str(tags.attrs["alt"]).lower():
                img_with_alt_logo.append(trim_tags(tags))
            elif not str(tags.attrs["alt"][0]).isupper():
                img_alt_starts_lower.append(trim_tags(tags))
    url_result["Img tags without alt attribute"] = img_tags_wo_alt_attr if len(img_tags_wo_alt_attr) > 0 else "None"
    url_result["Img tags with empty or none alt text"] = img_tags_with_empty_alt_txt if len(
        img_tags_with_empty_alt_txt) > 0 else "None"
    url_result["Img with alt LOGO"] = img_with_alt_logo if len(img_with_alt_logo) > 0 else "None"
    url_result["Img with lower case initial"] = img_alt_starts_lower if len(img_alt_starts_lower) > 0 else "None"
    return url_result


def check_resp_code(resp):
    url_result = {}
    url_result["Redirection_depth"] = len(resp.history)
    if len(resp.history) > 0:
        url_result["Response_code"] = resp.history[-1].status_code
        url_result["Redirected_to_url"] = resp.url
    else:
        url_result["Response_code"] = resp.status_code
        url_result["Redirected_to_url"] = "No redirection"
    return url_result


def check_h_tag_presence(h_tags):
    url_result = {}
    heads = 0
    keys = h_tags[0].keys()
    longer_headings = []
    for key in keys:
        heads = heads + len(h_tags[0][key])
        if len(h_tags[0][key]) > 0:
            for heading in h_tags[0][key]:
                if len(heading.text.split(" ")) > 10:
                    longer_headings.append(heading.text)

    url_result["Heading longer than 10 words in length"] = longer_headings if len(longer_headings) else "Heading lengths are fine"

    if heads > 0:
        url_result["Has headings"] = heads
    else:
        url_result["Has headings"] = 0
    return url_result


def check_image_size(img_tags, bg_images, url):
    url_result = {}
    inner_img_with_larger_size = []
    bg_img_with_larger_size = []
    # import urllib.request
    # from urllib.parse import urlparse
    parsed = urlparse(url)
    size_in_kb = 0
    for tags in img_tags:
        if "src" in tags.attrs and len(tags.attrs["src"]) > 1 and tags.attrs["src"] != "./" \
                and tags.attrs["src"] != ".#" and tags.attrs["src"] != "" and tags.attrs["src"] != " " \
                and tags.attrs["src"] != "http" and tags.attrs["src"] != "http:":
            if str(tags.attrs["src"]).startswith("/"):
                uri = parsed.scheme + "://" + parsed.netloc
                uri = uri + str(tags.attrs["src"])
            else:
                uri = str(tags.attrs["src"])
            try:
                file = urllib.request.urlopen(uri)
                size = file.headers.get("content-length")
                file.close()
                size_in_kb = int(size)/1000
                if size_in_kb > 50:
                    inner_img_with_larger_size.append(uri)
            except:
                inner_img_with_larger_size.append("Unable to determine size of "+str(uri))
    for tags in bg_images:
        img_uri = str(tags)
        img_uri = img_uri[img_uri.index('style="background-image:url(') + len('style="background-image:url('):len(img_uri) - 9]
        if str(img_uri).startswith("/"):
            uri = parsed.scheme + "://" + parsed.netloc
            uri = uri + str(img_uri)
        else:
            uri = str(img_uri)
        try:
            file = urllib.request.urlopen(uri)
            size = file.headers.get("content-length")
            file.close()
            size_in_kb = int(size) / 1000
            if size_in_kb > 50:
                bg_img_with_larger_size.append(uri)
        except:
            bg_img_with_larger_size.append("Unable to determine size of "+str(uri))
    img_with_larger_size = inner_img_with_larger_size + bg_img_with_larger_size
    url_result["Image size greater than 50KB"] = img_with_larger_size if len(img_with_larger_size) > 0 else "None"
    return url_result


def check_link_without_content(all_anchors):
    links_without_content = []
    url_result = {}
    for link in all_anchors:
        if len(link.text) == 0:
            if len(link.find_all(recursive=False)) == 0:
                if "href" in link.attrs and link.attrs["href"] == "javascript:void(0)":
                    pass
                else:
                    links_without_content.append(trim_tags(link))
    url_result["Links without content"] = links_without_content if len(links_without_content) > 0 else "None"
    return url_result


def check_node_urls(all_links):
    node_urls = []
    url_result = {}
    for link in all_links:
        url = link.attrs["href"]
        if r"/node/" in str(url):
            url_result['Node Urls on page'] = "Present"
            node_urls.append(url)
        else:
            url_result['Node Urls on page'] = "No node URLs on this page"
    url_result["List of Node urls on page"] = node_urls if len(node_urls) > 0 else "None"

    return url_result


def check_h_tag_hierarchy(soup):
    url_result = {}
    missing_headings = []
    tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
    all_headings = soup.find_all(tags)

    if len(all_headings) == 1:
        if all_headings[0].name != 'h1':

            s = "heading should be h1"
            missing_headings.append(s)
    x = 0
    while x < len(all_headings) - 1:
        if x == 0 and all_headings[x].name != 'h1':
            s = all_headings[x].name + " is the first heading. It should be h1 -> h2 -> h3 and so on"
            missing_headings.append(s)
            y = 1
            while y < int(all_headings[x].name[1]):

                s = "h" + str(y) + " is missing before " + all_headings[x].text
                missing_headings.append(s)
                y = y + 1

        if all_headings[x].name[1] != all_headings[x + 1].name[1]:
            if int(all_headings[x + 1].name[1]) > int(all_headings[x].name[1]) + 1:
                y = int(all_headings[x].name[1]) + 1
                while y < int(all_headings[x + 1].name[1]):
                    s = "h" + str(y) + " is missing after " + all_headings[x].text
                    missing_headings.append(s)
                    y = y + 1
            if int(all_headings[x + 1].name[1]) <= int(all_headings[x].name[1]):
                pass
        x = x + 1
    url_result["H tags hierarchy"] = missing_headings if len(missing_headings) > 0 else "Correct hierarchy is being followed"
    return url_result


def check_duplicate_descriptions(meta_desc, unique_desc):
    url_result = {}
    duplicate = False
    # print("checking duplicate meta desc")
    if meta_desc[0].attrs['content'] not in unique_desc:
        unique_desc.append(meta_desc[0].attrs['content'])
    else:
        duplicate = True

    if duplicate:
        url_result["Duplicate meta description on this page"] = "Present"
    else:
        url_result["Duplicate meta description on this page"] = "No"

    return url_result


def check_duplicate_page_title(title, unique_title):
    url_result = {}
    duplicate = False
    # print("checking duplicate titles")
    if title[0].text not in unique_title:
        unique_title.append(title[0].text)
    else:
        duplicate = True

    if duplicate:
        url_result["Duplicate title on this page"] = "Present"
    else:
        url_result["Duplicate title on this page"] = "No"

    return url_result


def check_natural_language(html):
    url_result = {}
    if "lang" in html.attrs and len(str(html.attrs["lang"])) > 0:
        url_result["Natural language"] = "Defined"
    else:
        url_result["Natural language"] = "Not defined"

    return url_result


def check_form_labels(forms, soup):
    # print(forms)
    url_result = {}
    controls = []
    missing_names = []
    for form in forms:
        # print("checking form :" +str(form))
        controls = form.find_all("input")
        controls.extend(form.find_all("textarea"))
        controls.extend(form.find_all("select"))
        # print("Controls " + str(controls))
        if len(controls) > 0:
            for control in controls:
                attribute_count = 0
                label_count = 0
                if "title" in control.attrs or "aria-label" in control.attrs or "aria-labelledby" in control.attrs:
                    attribute_count = attribute_count + 1
                if "id" in control.attrs:
                    labels = soup.find_all("label", {"id":control.attrs["id"]})
                    if len(labels) > 0:
                        label_count = label_count + 1
                        # print("label is present")
                if attribute_count == 0 and label_count == 0:
                    if "type" in control.attrs and "hidden" not in control.attrs["type"]:
                        missing_names.append(trim_tags(control))
    url_result["Form controls not readable by screen reader"] = missing_names if len(missing_names) > 0 else "No missing labels"
    return url_result


# this function checks if an inline style is being used on the page
def check_inline_style(soup):
    tags = soup.find_all()
    url_result = {}
    in_line_style = soup.find_all(style=True)
    style_tags = soup.find_all("style")

    url_result["Tags containing in-line style"] = in_line_style if len(in_line_style) > 0 else "No such tags"
    url_result["Style tags present on page"] = style_tags if len(style_tags) > 0 else "No style tags"

    return url_result


# check space before closing </p> tag
def check_space_before_closing_p(soup):
    p_ending_with_space = []
    longer_p_text = []
    url_result = {}
    p_text = soup.find_all("p", text=True)
    for tag in p_text:
        val = str(tag.text)
        words = val.split(" ")
        if len(words) > 100:
            longer_p_text.append(val[:20])
        if (val != " " or val != "&nbsp;") and (val.endswith(" ") or val.endswith("&nbsp;")):
            p_ending_with_space.append(trim_tags(tag))
    url_result["Space before a </p> tag"] = p_ending_with_space if len(p_ending_with_space) > 0 else "No such p tags"
    url_result["Paragraph longer than 100 words"] = longer_p_text if len(longer_p_text) > 0 else "No such p tags"
    return url_result


# check internal links getting open in a new tab or window
def check_internal_links_opening_in_new_window(all_href, url):
    parsed = urlparse(url)
    scheme = parsed.scheme
    host = parsed.netloc
    url_result = {}
    internal_link_in_new_window = []
    for anchor in all_href:
        if (str(anchor.attrs["href"]).startswith("/") or str(anchor.attrs["href"]).startswith(scheme+"://"+host)) and \
                ".pdf" not in str(anchor.attrs["href"]).lower():
            if "target" in anchor.attrs and anchor.attrs["target"] == "_blank":
                internal_link_in_new_window.append(str(anchor))
    url_result["Internal links getting open in a new window"] = internal_link_in_new_window if len(internal_link_in_new_window) > 0 else "No such internal links"
    return url_result


# check if disclaimer is present for share price or chart or not

def check_share_price_chart_disclaimer(soup, driver):
    iframes = soup.find_all("iframe")

    url_result = {}
    for frame in iframes:
        if "id" in frame.attrs:
            xpath = "//iframe[@id='{frame_id}']".format(frame_id=frame.attrs["id"])

            driver.switch_to.frame(driver.find_element_by_xpath(xpath))

            frame_soup = bs(driver.page_source, "html.parser")

            disclaimer_span = frame_soup.find_all("span", {"id": "disclaimer_MDA_Description"})
            if len(disclaimer_span) > 0:

                url_result["Disclaimer is present"] = "Disclaimer is present"
            else:
                # print("Not found, switching to another frame")
                driver.switch_to.parent_frame()
    return url_result


def check_table_headers(tables):
    missing_headers = []
    url_result = {}
    for table in tables:
        header_count = 0
        role_count = 0
        thead = table.find("thead")
        if "role" in table.attrs and table.attrs["role"] == "presentation":
            role_count = role_count + 1
        if thead is not None:
            headings = thead.find_all("th")
            if len(headings) > 0:
                header_count = header_count + 1

        else:
            rows = table.find_all("tr")
            for row in rows:
                headings = row.find_all("th")
                if len(headings) > 0:
                    header_count = header_count + 1

        if header_count == 0 and role_count == 0:
            missing_headers.append(trim_tags(table))
    url_result["Row and column headers and role are missing"] = missing_headers if len(missing_headers) else "None"
    return url_result


def check_tables_in_iframes(iframes, driver):
    # print(iframes)
    url_result = {}
    for frame in iframes:
        if "id" in frame.attrs:
            xpath = "//iframe[@id='{frame_id}']".format(frame_id=frame.attrs["id"])
            driver.switch_to.frame(driver.find_element_by_xpath(xpath))
            frame_soup = bs(driver.page_source, "html.parser")
            tables = frame_soup.find_all("table")
            if len(tables) > 0:
                url_result.update(check_table_headers(tables))
            driver.switch_to.parent_frame()

    return url_result


def check_pdf_title(url):
    url_result = {}
    try:
        resp = requests.get(url, headers=random_headers())
        # print(type(resp.status_code))
        if resp.status_code == 200 or resp.status_code == 301 or resp.status_code == 302:

            file = io.BytesIO(resp.content)
            reader = PdfFileReader(file)
            info_keys = list(reader.documentInfo.keys())
            if "/Title" in info_keys or "Title" in info_keys:
                title = reader.documentInfo["/Title"]
                # print(title)
                if len(title) == 0:
                    url_result["PDF Document Title"] = "Missing"
                else:
                    url_result["PDF Document Title"] = "Title present"
            else:
                url_result["PDF Document Title"] = "Missing"
    except:
        url_result["PDF Document Title"] = "PDF is not accessible, please check the Response Code Column"
    return url_result


def shfjh():
    pass