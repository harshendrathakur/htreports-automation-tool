import requests
from randon_headers import random_headers
from urllib.parse import quote


class GetResponse(object):

    def get_url(self, url, auth_username, auth_password, host_scheme, ses):
        # print("about to check response")
        if len(auth_username) > 0:
            is_auth = True
            auths = auth_username + ":" + auth_password + "@"
            if auths not in url:
                if host_scheme == "https":
                    req_url = url[:8] + auths + url[8:]
                    # print("formed url" + str(req_url))
                    url = req_url
                else:
                    req_url = url[:7] + auths + url[7:]
                    # print("formed url" + str(req_url))
                    url = req_url
        try:
            if ses is None:
                resp = requests.get(url, allow_redirects=True, headers=random_headers())
            else:
                resp = ses.get(url, allow_redirects=True, headers=random_headers())
            # print(resp.status_code)
            return resp
        except requests.exceptions.SSLError:
            return "SSL Error"
        except requests.exceptions.ConnectionError:
            return "Connection Error"
        except requests.exceptions.ConnectTimeout:
            return "Connect Timeout"
        except requests.exceptions.MissingSchema:
            return "Missing Schema error"
        except requests.exceptions.InvalidURL:
            return "Invalid URL error"
        except requests.exceptions.BaseHTTPError:
            return "BaseHTTP error"
        except requests.exceptions.Timeout:
            return "Timeout"
        except requests.exceptions.ReadTimeout:
            return "Read Timeout error"
        except requests.exceptions:
            return "Not accessible"

    def form_url(self, url, auth_uname, auth_password, host_scheme):
        # print("forming url")
        if len(auth_uname) > 0:
            is_auth = True
            auth_uname = quote(auth_uname)
            auth_password = quote(auth_password)
            # print(auth_password)

            auths = auth_uname + ":" + auth_password + "@"
            if auths not in url:
                if host_scheme == "https":
                    req_url = url[:8] + auths + url[8:]
                    url = req_url
                else:
                    req_url = url[:7] + auths + url[7:]
                    # print("formed url" + str(req_url))
                    url = req_url
        return url